//
//  GameWorldsFetcher.m
//  GameWorlds
//
//  Created by John Raymund Catahay on 13/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import "GameWorldsFetcher.h"
#import "AFNetworking.h"
#import "Constants.h"

@implementation GameWorldsFetcher

- (void)getGameWorldsWithCredentials:(NSString *)email withPassword:(NSString*)password{
    
    NSString *deviceType = [NSString stringWithFormat:@"%@ - %@ %@", [[UIDevice currentDevice] model], [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]];
    NSString *deviceId = [[NSUUID UUID] UUIDString];
    NSDictionary *parameters = @{@"login":email, @"password":password, @"deviceType":deviceType, @"deviceId":deviceId};
    
    NSString *url = kFetchGameWorldsAPIURL;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFPropertyListResponseSerializer serializer];
    
    [manager GET:url
      parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSArray *worldsDictionary = [responseObject objectForKey:@"allAvailableWorlds"];
             [_delegate didReceivedGameWorlds:worldsDictionary withError:nil];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [_delegate didReceivedGameWorlds:nil withError:error];;
    }];
}

@end
