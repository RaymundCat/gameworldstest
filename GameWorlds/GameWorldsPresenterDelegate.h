//
//  GameWorldsPresenterDelegate.h
//  GameWorlds
//
//  Created by John Raymund Catahay on 11/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GameWorldsPresenterDelegate <NSObject>

- (void)didStartFetchingGameWorlds;
- (void)didReceieveGameWorlds:(NSArray *)worlds;

@end
