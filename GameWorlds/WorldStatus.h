//
//  WorldStatus.h
//  GameWorlds
//
//  Created by John Raymund Catahay on 12/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorldStatus : NSObject

@property NSString* statusDescription;
@property int id;

@end
