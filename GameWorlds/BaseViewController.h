//
//  BaseViewController.h
//  GameWorlds
//
//  Created by John Raymund Catahay on 13/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (void)showLoading: (NSString *)message;
- (void)hideLoading;
- (void)showToast: (NSString *)message;


@end
