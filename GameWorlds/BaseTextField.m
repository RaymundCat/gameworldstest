//
//  BaseTextField.m
//  GameWorlds
//
//  Created by John Raymund Catahay on 12/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import "BaseTextField.h"

@implementation BaseTextField


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    self.layer.borderColor = [[UIColor blackColor] CGColor];
    self.layer.borderWidth = 2;
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
}

@end
