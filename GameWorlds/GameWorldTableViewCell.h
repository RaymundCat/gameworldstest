//
//  GameWorldTableViewCell.h
//  GameWorlds
//
//  Created by John Raymund Catahay on 12/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "World.h"

@interface GameWorldTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *worldTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *worldStatusLabel;

- (void)updateCellViewWithWorld:(World*)world;

@end
