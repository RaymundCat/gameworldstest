//
//  GameWorldsPresenter.h
//  GameWorlds
//
//  Created by John Raymund Catahay on 11/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameWorldsPresenterDelegate.h"
#import "GameWorldsFetcher.h"
#import "GameWorldsFetcherDelegate.h"
#import "GameWorldIterator.h"

@interface GameWorldsPresenter : NSObject<GameWorldsFetcherDelegate>

@property id<GameWorldsPresenterDelegate> delegate;
@property GameWorldsFetcher *fetcher;
@property GameWorldIterator *iterator;

- (void)didPressSubmit:(NSString* )email withPassword:(NSString*)password;

@end
