//
//  GameWorldsViewController.m
//  GameWorlds
//
//  Created by John Raymund Catahay on 11/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import "GameWorldsViewController.h"
#import "GameWorldsFetcher.h"
#import "GameWorldIterator.h"

@interface GameWorldsViewController(){
    GameWorldsPresenter* _presenter;
    NSArray* _worlds;
}

- (void)updateTableWithCount:(int)tableCount;

@end

@implementation GameWorldsViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    _presenter = [[GameWorldsPresenter alloc] init];
    _presenter.delegate = self;
    
    _presenter.fetcher = [[GameWorldsFetcher alloc]init];
    _presenter.fetcher.delegate = _presenter;
    
    _presenter.iterator = [[GameWorldIterator alloc]init];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 60;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_submitButton addTarget:self action:@selector(didPressSubmit:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didPressSubmit:(UIButton *)sender{
    [_presenter didPressSubmit:_emailTextField.text withPassword:_passwordTextField.text];
}

- (void)updateTableWithCount:(int)tableCount{
    if(tableCount < 1){
        [_tableView setHidden:YES];
    }else{
        [_tableView setHidden:NO];
    }
}

#pragma mark - Textfield Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([textField isKindOfClass:[BaseTextField class]]) {
        UITextField *nextField = [(BaseTextField*)textField nextField];
        if(nextField){
            dispatch_async(dispatch_get_main_queue(), ^{
                [nextField becomeFirstResponder];
            });
        }else{
            [textField resignFirstResponder];
        }
    }
    return YES;
}

#pragma mark - TableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    [self updateTableWithCount:_worlds.count];
    return _worlds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    GameWorldTableViewCell* cell = (GameWorldTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"GameWorldCellIdentifier"];
    [cell updateCellViewWithWorld:[_worlds objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - Presenter Delegates

- (void)didStartFetchingGameWorlds{
    [self showLoading:@"Fetching Game Worlds."];
}

- (void)didReceieveGameWorlds:(NSArray *)worlds{
    [self hideLoading];
    
    if ([worlds count] == 0) {
        [self showToast:@"No Worlds Loaded."];
    }
    
    _worlds = worlds;
    [_tableView reloadData];
}

@end
