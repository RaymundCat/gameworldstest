//
//  GameWorldsFetcher.h
//  GameWorlds
//
//  Created by John Raymund Catahay on 13/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameWorldsFetcherDelegate.h"
#import "AFNetworking.h"

@interface GameWorldsFetcher : NSObject

@property id<GameWorldsFetcherDelegate> delegate;
- (void)getGameWorldsWithCredentials:(NSString *)email withPassword:(NSString*)password;

@end
