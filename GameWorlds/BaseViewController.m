//
//  BaseViewController.m
//  GameWorlds
//
//  Created by John Raymund Catahay on 13/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import "BaseViewController.h"
#import "MBProgressHUD.h"

@implementation BaseViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    //setup appearance
    if(self.navigationController){
        [self.navigationController.navigationBar setBarTintColor:[UIColor yellowColor]];
        [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setShadowImage:[[UIImage alloc]init]];
    }
}

- (void)showLoading: (NSString *)message{
    [self hideLoading];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud.label setText:message];
}

- (void)hideLoading{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)showToast: (NSString *)message{
    [self hideLoading];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    [hud.label setText:message];
    
    //hide the hub in 2 secs
    hud.margin = 10.0f;
    hud.yOffset = 150.0f;
    
    [hud hideAnimated:YES afterDelay:3];
}

@end
