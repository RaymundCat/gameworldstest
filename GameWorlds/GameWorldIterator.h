//
//  GameWorldIterator.h
//  GameWorlds
//
//  Created by John Raymund Catahay on 12/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameWorldIterator : NSObject

- (NSArray *)worldsFromDictArray:(NSArray *)dictarray;

@end
