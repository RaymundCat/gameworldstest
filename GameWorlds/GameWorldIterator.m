//
//  GameWorldIterator.m
//  GameWorlds
//
//  Created by John Raymund Catahay on 12/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import "GameWorldIterator.h"
#import "World.h"
#import "WorldStatus.h"

@implementation GameWorldIterator

- (NSArray *)worldsFromDictArray:(NSArray *)dictarray{
    NSMutableArray* worlds = [[NSMutableArray alloc] init];
    
    for (NSDictionary *worldDict in dictarray) {
        World *world = [[World alloc]init];
        world.countryCode = [worldDict objectForKey:@"country"];
        world.id = (int)[worldDict objectForKey:@"id"];
        world.language = [worldDict objectForKey:@"language"];
        world.mapURL = [worldDict objectForKey:@"mapURL"];
        world.worldName = [worldDict objectForKey:@"name"];
        world.worldURL = [worldDict objectForKey:@"url"];
        
        WorldStatus* status = [[WorldStatus alloc]init];
        status.statusDescription = [[worldDict objectForKey:@"worldStatus"] objectForKey:@"description"];
        status.id = (int)[[worldDict objectForKey:@"worldStatus"] objectForKey:@"id"];
        world.status = status;
        
        [worlds addObject:world];
    }
    
    return worlds;
}

@end
