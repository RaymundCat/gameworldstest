//
//  Worlds.h
//  GameWorlds
//
//  Created by John Raymund Catahay on 11/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorldStatus.h"

@interface World : NSObject

@property NSString *countryCode;
@property int id;
@property NSString *language;
@property NSString *mapURL;
@property NSString *worldName;
@property NSString *worldURL;
@property WorldStatus *status;

@end
