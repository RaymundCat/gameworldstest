//
//  GameWorldsViewController.h
//  GameWorlds
//
//  Created by John Raymund Catahay on 11/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameWorldsPresenterDelegate.h"
#import "GameWorldTableViewCell.h"
#import "GameWorldsPresenter.h"
#import "BaseTextField.h"
#import "BaseViewController.h"

@interface GameWorldsViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource, GameWorldsPresenterDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
