//
//  GameWorldsPresenter.m
//  GameWorlds
//
//  Created by John Raymund Catahay on 11/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import "GameWorldsPresenter.h"
#import "GameWorldIterator.h"

@implementation GameWorldsPresenter 

- (void)didPressSubmit:(NSString *)email withPassword:(NSString *)password{
    [_fetcher getGameWorldsWithCredentials:email withPassword:password];
    [_delegate didStartFetchingGameWorlds];
}

#pragma mark - Fetcher Delegate

- (void)didReceivedGameWorlds:(NSArray *)gameWorlds withError:(NSError *)error{
    [_delegate didReceieveGameWorlds: [_iterator worldsFromDictArray:gameWorlds]];
}

@end
