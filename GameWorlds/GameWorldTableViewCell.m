//
//  GameWorldTableViewCell.m
//  GameWorlds
//
//  Created by John Raymund Catahay on 12/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import "GameWorldTableViewCell.h"

@implementation GameWorldTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if(selected){
        self.backgroundColor = [UIColor lightGrayColor];
    }else{
        self.backgroundColor = [UIColor darkGrayColor];
    }
}

- (void)updateCellViewWithWorld:(World *)world{
    
    [_worldTitleLabel setText:world.worldName];
    [_worldStatusLabel setText:world.status.statusDescription];
    
    [self layoutSubviews];
}

@end
