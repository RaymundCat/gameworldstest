//
//  GameWorldsFetcherDelegate.h
//  GameWorlds
//
//  Created by John Raymund Catahay on 13/08/2016.
//  Copyright © 2016 John Raymund Catahay. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GameWorldsFetcherDelegate <NSObject>

- (void)didReceivedGameWorlds:(NSArray *)gameWorlds withError:(NSError *)error;

@end
